import {
  Meteor
} from 'meteor/meteor';
import '../imports/api/tasks.js';

Meteor.startup(() => {
  process.env.MONGO_URL = "mongodb://localhost:27017/simple-react-todos"
});